/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooexamen1;

/**
 *
 * @author Hector Ramirez
 */
public class Contrato {

    private int clave;
    private String puesto;
    private float impuesto;

    public Contrato() {
        this.clave = 0;
        this.puesto = "";
        this.impuesto = 0.0f;
    }

    public Contrato(int clave, String puesto, float impuesto) {
        this.clave = clave;
        this.puesto = puesto;
        this.impuesto = impuesto;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(float impuesto) {
        this.impuesto = impuesto;
    }
    
}
