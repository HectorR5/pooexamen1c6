/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooexamen1;

/**
 *
 * @author Hector Ramirez
 */
public class Docente extends Empleado{

    private int nivel;
    private float horas;
    private float pagoHora;

    public Docente() {
        this.nivel = 0;
        this.horas = 0.0f;
        this.pagoHora = 0.0f;
    }

    public Docente(int nivel, float horas, float pagoHora, String nombre, String domicilio, Contrato contrato, int numEmp) {
        super(nombre, domicilio, contrato, numEmp);
        this.nivel = nivel;
        this.horas = horas;
        this.pagoHora = pagoHora;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }
    
    @Override
    public float calcularTotal() {
        float total=0.0f;
        if(this.nivel==0) total=(this.pagoHora * this.horas*1.35f)-this.calcularImpuesto();
        if(this.nivel==1) total=(this.pagoHora * this.horas*1.4f)-this.calcularImpuesto();
        if(this.nivel==2) total=(this.pagoHora * this.horas*1.5f)-this.calcularImpuesto();
        return total;
    }

    @Override
    public float calcularImpuesto() {
        return (this.pagoHora * this.horas)*this.contrato.getImpuesto()/100f;
    }
    
}
