/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooexamen1;

/**
 *
 * @author Hector Ramirez
 */
public abstract class Empleado {
    
    protected String nombre;
    protected String domicilio;
    protected Contrato contrato;
    protected int numEmp;

    public Empleado() {
        this.nombre = "";
        this.domicilio = "";
        this.contrato = new Contrato();
        this.numEmp = 0;
    }

    public Empleado(String nombre, String domicilio, Contrato contrato, int numEmp) {
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.contrato = contrato;
        this.numEmp = numEmp;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public int getNumEmp() {
        return numEmp;
    }

    public void setNumEmp(int numEmp) {
        this.numEmp = numEmp;
    }
    
    public abstract float calcularTotal();
    
    public abstract float calcularImpuesto();
    
}
