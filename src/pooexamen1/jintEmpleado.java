/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooexamen1;

import javax.swing.JOptionPane;

/**
 *
 * @author Hector Ramirez
 */
public class jintEmpleado extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintEmpleado
     */
    public jintEmpleado() {
        initComponents();
        resize(580,550);
        this.deshabilitar();
    }
    
    public void deshabilitar(){
        this.txtClave.setEnabled(false);
        this.txtDom.setEnabled(false);
        this.txtHoras.setEnabled(false);
        this.txtImpuesto.setEnabled(false);
        this.txtNombre.setEnabled(false);
        this.txtNumEmp.setEnabled(false);
        this.txtPagoHora.setEnabled(false);
        this.txtPagoAd.setEnabled(false);
        this.txtPagoBase.setEnabled(false);
        this.txtPagoIm.setEnabled(false);
        this.txtTotal.setEnabled(false);
        this.cmbNivel.setEnabled(false);
        this.txtPuesto.setEnabled(false);
        this.btnGuardar.setEnabled(false);
        this.btnMostrar.setEnabled(false);
    }
    
    public void habilitar(){
        this.txtClave.setEnabled(!false);
        this.txtDom.setEnabled(!false);
        this.txtHoras.setEnabled(!false);
        this.txtImpuesto.setEnabled(!false);
        this.txtNombre.setEnabled(!false);
        this.txtNumEmp.setEnabled(!false);
        this.txtPagoHora.setEnabled(!false);
        this.cmbNivel.setEnabled(!false);
        this.txtPuesto.setEnabled(!false);
        this.btnGuardar.setEnabled(!false);
    }
    
    public void limpiar(){
        this.txtClave.setText("");
        this.txtDom.setText("");
        this.txtHoras.setText("");
        this.txtImpuesto.setText("");
        this.txtNombre.setText("");
        this.txtNumEmp.setText("");
        this.txtPagoHora.setText("");
        this.txtPagoAd.setText("");
        this.txtPagoBase.setText("");
        this.txtPagoIm.setText("");
        this.txtTotal.setText("");
        this.cmbNivel.setSelectedIndex(0);
        this.txtPuesto.setText("");
    }
    
    public boolean validar(){
        boolean exito = false;
        if (this.txtClave.getText().equals("")) exito=true;
        if (this.txtDom.getText().equals("")) exito=true;
        if (this.txtHoras.getText().equals("")) exito=true;
        if (this.txtImpuesto.getText().equals("")) exito=true;
        if (this.txtNombre.getText().equals("")) exito=true;
        if (this.txtNumEmp.getText().equals("")) exito=true;
        if (this.txtPagoHora.getText().equals("")) exito=true;
        if (this.txtPuesto.getText().equals("")) exito=true;
        return exito;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtNumEmp = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDom = new javax.swing.JTextField();
        txtPuesto = new javax.swing.JTextField();
        txtHoras = new javax.swing.JTextField();
        txtPagoHora = new javax.swing.JTextField();
        txtClave = new javax.swing.JTextField();
        cmbNivel = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        txtPagoIm = new javax.swing.JTextField();
        txtPagoAd = new javax.swing.JTextField();
        txtPagoBase = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        btnCerrar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        txtImpuesto = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        getContentPane().setLayout(null);

        jLabel1.setText("%Impuesto");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(70, 180, 65, 16);

        jLabel2.setText("Puesto");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(80, 160, 50, 16);

        jLabel3.setText("Domicilio");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(70, 120, 60, 16);

        jLabel4.setText("Nombre");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(80, 100, 45, 16);

        jLabel5.setText("Num. Empleado");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 60, 100, 16);

        jLabel6.setText("Nivel");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(90, 230, 27, 16);

        jLabel7.setText("Pago por Hora");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(40, 260, 82, 16);

        jLabel8.setText("Horas Trabajadas");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(20, 290, 110, 16);

        txtNumEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumEmpActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumEmp);
        txtNumEmp.setBounds(140, 60, 190, 22);

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        getContentPane().add(txtNombre);
        txtNombre.setBounds(140, 100, 190, 22);

        txtDom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDomActionPerformed(evt);
            }
        });
        getContentPane().add(txtDom);
        txtDom.setBounds(140, 120, 190, 22);

        txtPuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPuestoActionPerformed(evt);
            }
        });
        getContentPane().add(txtPuesto);
        txtPuesto.setBounds(140, 160, 190, 22);

        txtHoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHorasActionPerformed(evt);
            }
        });
        getContentPane().add(txtHoras);
        txtHoras.setBounds(140, 290, 190, 22);

        txtPagoHora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoHoraActionPerformed(evt);
            }
        });
        getContentPane().add(txtPagoHora);
        txtPagoHora.setBounds(140, 260, 190, 22);

        txtClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClaveActionPerformed(evt);
            }
        });
        getContentPane().add(txtClave);
        txtClave.setBounds(140, 40, 190, 22);

        cmbNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pre escolar", "Primaria", "Secundaria" }));
        getContentPane().add(cmbNivel);
        cmbNivel.setBounds(140, 230, 190, 22);

        jPanel1.setBackground(new java.awt.Color(255, 255, 0));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 13))); // NOI18N
        jPanel1.setLayout(null);

        jLabel9.setText("Pago Impuestos (-)");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(10, 80, 120, 16);

        jLabel11.setText("Pago Adicional (+)");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(10, 50, 110, 16);

        jLabel12.setText("Pago Total");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(50, 110, 70, 16);

        jLabel13.setText("Total");
        jPanel1.add(jLabel13);
        jLabel13.setBounds(90, 20, 29, 16);

        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel1.add(txtTotal);
        txtTotal.setBounds(130, 110, 150, 22);

        txtPagoIm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoImActionPerformed(evt);
            }
        });
        jPanel1.add(txtPagoIm);
        txtPagoIm.setBounds(130, 80, 150, 22);

        txtPagoAd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoAdActionPerformed(evt);
            }
        });
        jPanel1.add(txtPagoAd);
        txtPagoAd.setBounds(130, 50, 150, 22);

        txtPagoBase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoBaseActionPerformed(evt);
            }
        });
        jPanel1.add(txtPagoBase);
        txtPagoBase.setBounds(130, 20, 150, 22);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(70, 330, 300, 150);

        jLabel10.setText("Clave Contrato");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(40, 40, 84, 16);

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(400, 310, 130, 30);

        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(390, 170, 140, 50);

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(390, 40, 140, 50);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(400, 270, 130, 30);

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(390, 100, 140, 50);

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(400, 230, 130, 30);

        txtImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestoActionPerformed(evt);
            }
        });
        getContentPane().add(txtImpuesto);
        txtImpuesto.setBounds(140, 180, 190, 22);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNumEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumEmpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumEmpActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtDomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDomActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDomActionPerformed

    private void txtPuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPuestoActionPerformed

    private void txtHorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHorasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHorasActionPerformed

    private void txtPagoHoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoHoraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoHoraActionPerformed

    private void txtClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClaveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClaveActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void txtPagoImActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoImActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoImActionPerformed

    private void txtPagoAdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoAdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoAdActionPerformed

    private void txtPagoBaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoBaseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoBaseActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        em = new Docente();
        co = new Contrato();
        this.limpiar();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        boolean error=false;
        if(this.validar()==false){
            try{
            co.setClave(Integer.parseInt(this.txtClave.getText()));
            co.setPuesto(this.txtPuesto.getText());
            co.setImpuesto(Float.parseFloat(this.txtImpuesto.getText()));
            em.setContrato(co);
            em.setDomicilio(this.txtDom.getText());
            em.setHoras(Float.parseFloat(this.txtHoras.getText()));
            em.setNivel(this.cmbNivel.getSelectedIndex());
            em.setNombre(this.txtNombre.getText());
            em.setNumEmp(Integer.parseInt(this.txtNumEmp.getText()));
            em.setPagoHora(Float.parseFloat(this.txtPagoHora.getText()));
            }
            catch(NumberFormatException e) {
                error=true;
                JOptionPane.showMessageDialog(this, "Surgio un error al capturar" + e.getMessage());
                this.deshabilitar();
                this.limpiar();
            }
            if(error==false){
                this.btnMostrar.setEnabled(true);
                JOptionPane.showMessageDialog(this, "Se guardó la informacion con exito");
            }
        }
        else{
            JOptionPane.showMessageDialog(this, "Falto capturar información");
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void txtImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestoActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        int nivel=em.getNivel();
        this.txtDom.setText(em.getDomicilio());
        this.txtClave.setText(String.valueOf(em.contrato.getClave()));
        this.txtImpuesto.setText(String.valueOf(em.contrato.getImpuesto()));
        this.txtPuesto.setText(em.contrato.getPuesto());
        this.txtHoras.setText(String.valueOf(em.getHoras()));
        this.txtPagoHora.setText(String.valueOf(em.getPagoHora()));
        this.txtNumEmp.setText(String.valueOf(em.getNumEmp()));
        this.txtNombre.setText(em.getNombre());
        this.txtHoras.setText(String.valueOf(em.getHoras()));
        this.txtPagoBase.setText(String.valueOf(em.getHoras()*em.getPagoHora()));
        this.txtPagoIm.setText(String.valueOf(em.calcularImpuesto()));
        switch(nivel){
            case 0: this.txtPagoAd.setText(String.valueOf(em.getHoras()*em.getPagoHora()*1.35f)); break;
            case 1: this.txtPagoAd.setText(String.valueOf(em.getHoras()*em.getPagoHora()*1.4f)); break;
            case 2: this.txtPagoAd.setText(String.valueOf(em.getHoras()*em.getPagoHora()*1.5f)); break;
        }
        this.txtTotal.setText(String.valueOf(em.calcularTotal()));
        
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int opcion = 0;
        opcion = JOptionPane.showConfirmDialog(this, "Deseas salir?", "Pago Empleados", JOptionPane.YES_NO_OPTION);
        if (opcion == JOptionPane.YES_OPTION){
            this.dispose();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbNivel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtClave;
    private javax.swing.JTextField txtDom;
    private javax.swing.JTextField txtHoras;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumEmp;
    private javax.swing.JTextField txtPagoAd;
    private javax.swing.JTextField txtPagoBase;
    private javax.swing.JTextField txtPagoHora;
    private javax.swing.JTextField txtPagoIm;
    private javax.swing.JTextField txtPuesto;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
    private Docente em;
    private Contrato co;
}
